# asio

Cross-platform C++ library for network and low-level I/O. https://www.boost.org/doc/libs/release/libs/asio/

* https://think-async.com
* https://boost.org/doc/libs/1_70_0/doc/html/boost_asio/overview/networking/iostreams.html (!version #)
* https://boost.org
* https://theboostcpplibraries.com/